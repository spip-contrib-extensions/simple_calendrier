<?php

/***************************************************************************\
 * Adapté du plugin breves.
 * Raison : la mécanique de base de SPIP unset les id_rubrique ...
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action d'édition d'un évènement dans la base de données dont
 * l'identifiant est donné en paramètre de cette fonction ou
 * en argument de l'action sécurisée
 *
 * Si aucun identifiant n'est donné, on crée alors un nouvel évènement.
 *
 * @param null|int $arg
 *     Identifiant de l'évènement. En absence utilise l'argument
 *     de l'action sécurisée.
 * @return array
 *     Liste : identifiant de l'évènement, texte d'erreur éventuel
 **/
function action_editer_evenement_dist($arg = null) {

	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	// Envoi depuis le formulaire d'edition d'un evenement
	if (!$id_evenement = intval($arg)) {
		$id_evenement = evenement_inserer(_request('id_rubrique'));
	}

	if (!$id_evenement) {
		return [0, ''];
	} // erreur

	$err = evenement_modifier($id_evenement);

	return [$id_evenement, $err];
}


/**
 * Insertion d'un évènement dans une rubrique
 *
 * @pipeline_appel pre_insertion
 * @pipeline_appel post_insertion
 *
 * @param int $id_rubrique
 *     Identifiant de la rubrique
 * @param array|null $set
 * @return int
 *     Identifiant du nouvel évènement.
 */
function evenement_inserer($id_rubrique, $set = null) {

	include_spip('inc/rubriques');

	// Si id_rubrique vaut 0 ou n'est pas definie, c'est qu'on est en mode 'sans rubrique'
	if (!$id_rubrique = intval($id_rubrique)) {
		$id_rubrique = 0;
	}

	// La langue a la creation : c'est la langue de la rubrique
	$row = sql_fetsel('lang, id_secteur', 'spip_rubriques', 'id_rubrique=' . intval($id_rubrique));
	
	$lang = 'fr';
	$id_secteur = 0;
	if ($row) {
		$lang = $row['lang'];
		$id_secteur = intval($row['id_secteur']);
	}

	$champs = [
		'id_rubrique' => $id_rubrique,
		'id_secteur' => $id_secteur,
		'statut' => 'prepa',
		'lang' => $lang,
		'langue_choisie' => 'non'
	];

	if ($set) {
		$champs = array_merge($champs, $set);
	}

	// Envoyer aux plugins
	$champs = pipeline(
		'pre_insertion',
		[
			'args' => [
				'table' => 'spip_evenements',
			],
			'data' => $champs
		]
	);
	$id_evenement = sql_insertq('spip_evenements', $champs);
	pipeline(
		'post_insertion',
		[
			'args' => [
				'table' => 'spip_evenements',
				'id_objet' => $id_evenement
			],
			'data' => $champs
		]
	);

	return $id_evenement;
}


/**
 * Modifier un évènement en base
 *
 * @param int $id_evenement
 *     Identifiant de l'évènement à modifier
 * @param array|null $set
 *     Couples (colonne => valeur) de données à modifier.
 *     En leur absence, on cherche les données dans les champs éditables
 *     qui ont été postés (via _request())
 * @return string|null
 *     Chaîne vide si aucune erreur,
 *     Null si aucun champ à modifier,
 *     Chaîne contenant un texte d'erreur sinon.
 */
function evenement_modifier($id_evenement, $set = null) {

	include_spip('inc/modifier');
	include_spip('inc/filtres');
	$c = collecter_requests(
		// white list
		objet_info('evenement', 'champs_editables'),
		// black list
		['id_parent', 'statut'],
		// donnees eventuellement fournies
		$set
	);

	$id_rubrique = intval($c['id_rubrique']);

	$invalideur = '';
	$indexation = false;

	$row = sql_fetsel('statut, id_secteur, id_rubrique', 'spip_evenements', 'id_evenement=' . intval($id_evenement));
	
	// Si l'evenement est publie, invalider les caches et demander sa reindexation
	if ($row['statut'] == 'publie') {
		$invalideur = "id='evenement/$id_evenement'";
		$indexation = true;
	}

	if (
		$err = objet_modifier_champs(
			'evenement',
			$id_evenement,
			[
			'data' => $set,
			'nonvide' => ['titre' => 'Nouvel evenement ' . $id_evenement],
			'invalideur' => $invalideur,
			'indexation' => $indexation
			],
			$c
		)
	) {
		return $err;
	}


	$c = collecter_requests(['id_rubrique', 'statut'], [], $set);
	$err = evenement_instituer($id_evenement, $c);

	return $err;
}


/**
 * Instituer un évènement : modifier son statut ou son parent
 *
 * @pipeline_appel pre_insertion
 * @pipeline_appel post_insertion
 *
 * @param int $id_evenement
 *     Identifiant de l'évènement
 * @param array $c
 *     Couples (colonne => valeur) des données à instituer
 * @return string|null
 *     Null si aucun champ à modifier, chaîne vide sinon.
 */
function evenement_instituer($id_evenement, $c) {
	//print_r($c); die();
	$champs = [];

	// Changer le statut de l'evenement ?
	$row = sql_fetsel('statut, id_rubrique, lang, langue_choisie', 'spip_evenements', 'id_evenement=' . intval($id_evenement));
	$id_rubrique_old = $row['id_rubrique'];

	$statut_ancien = $statut = $row['statut'];
	$langue_old = $row['lang'];
	$langue_choisie_old = $row['langue_choisie'];

	if (
		isset($c['statut'])
		and $c['statut']
		and $c['statut'] != $statut
		and autoriser('publierdans', 'rubrique', $id_rubrique_old)
	) {
		$statut = $champs['statut'] = $c['statut'];
	}

	// Changer de rubrique ?
	if (isset($c['id_rubrique'])) {
		$id_rubrique = intval($c['id_rubrique']);
		if ($id_rubrique != $id_rubrique_old) {
			$champs['id_rubrique'] = $id_rubrique;

			// Fixer le secteur
			$champs['id_secteur'] = 0;
			if ($id_rubrique > 0) {
				$id_secteur = sql_getfetsel('id_secteur', 'spip_rubriques', 'id_rubrique=' . $id_rubrique);
				$champs['id_secteur'] = intval($id_secteur);
			}
			
			// - changer sa langue (si heritee)
			if ($langue_choisie_old != 'oui') {
				$lang = sql_getfetsel('lang', 'spip_rubriques', 'id_parent=0 AND id_rubrique=' . intval($id_rubrique));
				if ($lang != $langue_old) {
					$champs['lang'] = $lang;
				}
			}
			// si l'evenement est publie
			// et que le demandeur n'est pas admin de la rubrique
			// repasser l'evenement en statut 'prop'.
			if ($statut == 'publie') {
				if (!autoriser('publierdans', 'rubrique', $id_rubrique)) {
					$champs['statut'] = $statut = 'prop';
				}
			}
		}
	}

	// Envoyer aux plugins
	$champs = pipeline(
		'pre_edition',
		[
			'args' => [
				'table' => 'spip_evenements',
				'id_objet' => $id_evenement,
				'action' => 'instituer',
				'statut_ancien' => $statut_ancien,
			],
			'data' => $champs
		]
	);

	if (!$champs) {
		return;
	}

	sql_updateq('spip_evenements', $champs, 'id_evenement=' . intval($id_evenement));

	//
	// Post-modifications
	//

	// Invalider les caches
	include_spip('inc/invalideur');
	suivre_invalideur("id='evenement/$id_evenement'");

	// Au besoin, changer le statut des rubriques concernees
	include_spip('inc/rubriques');
	calculer_rubriques_if($id_rubrique, $champs, $statut_ancien);

	// Pipeline
	pipeline(
		'post_edition',
		[
			'args' => [
				'table' => 'spip_evenements',
				'id_objet' => $id_evenement,
				'action' => 'instituer',
				'statut_ancien' => $statut_ancien,
			],
			'data' => $champs
		]
	);


	// Notifications
	if ($notifications = charger_fonction('notifications', 'inc')) {
		$notifications(
			'instituerevenement',
			$id_evenement,
			['statut' => $statut, 'statut_ancien' => $statut_ancien]
		);
	}

	return ''; // pas d'erreur
}
