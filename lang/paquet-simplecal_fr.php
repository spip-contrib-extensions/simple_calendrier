<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/simple_calendrier.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'simplecal_description' => 'Un simple calendrier pour planifier des évènements.',
	'simplecal_slogan' => 'Planifier simplement vos évènements.'
);
